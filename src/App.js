import React, { useState } from 'react';

// 1. import UIKit component
// 1. UIKit 구성 요소 import
import { MJTMainView, MJTRTSView, Mojitok } from '@mojitok/mojitok-uikit';
import '@mojitok/mojitok-uikit/dist/index.css';

const App = () => {
  const [text, setText] = useState('');
  const [isShow, setIsShow] = useState(false);
  const [stickerPreviewUrl, setStickerPreviewUrl] = useState();

  // 2-1. Initialize Mojitok module
  // 2-1. Mojitok 모듈 초기화
  Mojitok.setup(
    '{YOUR-APP-ID}', // Application ID
  );

  // 2-2. Choose one of our payment platforms, currently “EXIMBAY” is supported, will be expanded later.
  // 2-2. 결제 플랫폼 선택. 현재 "EXIMBAY" 만 지원되며 추후 지원 플랫폼을 추가할 계획입니다.
  Mojitok.setPaymentPlatform("EXIMBAY");

  // 2-3. Set position and size of store web popup.
  // 2-3. 스토어 웹 팝업의 위치와 크기 설정
  Mojitok.setStoreGeometry(/* {POS-LEFT} */0, /* {POS-TOP} */0, /* {SIZE-WIDTH} */360, /* {SIZE-HEIGHT} */640); // Position and size of store web popup

  // 3. Mojitok module login (async(recommended) example, synchronous implementation is also possible)
  // 3. Mojitok 모듈 로그인 (비동기(권장) 예시이며, 동기 구현도 가능합니다)
  Mojitok.login(
    '{YOUR-APP-TOKEN}', // Application Token
    '{YOUR-APP-USER-ID}', // User ID of your app
    (e) => {
      if (e == null) {
        // 3-a. on Success
        // 3-a. 성공 시
      } else {
        // 3-b. on Fail: If the e.code value is 4001, then please check the applicationId or the applicationToken
        // 3-b. 실패 시: e.code 값이 4001이라면 applicationId 또는 Token을 확인해주세요
      }
    }
  );

  // 4. Write <choice> Callback function to be used for MJTMainView, MJTRTSView
  // 4. MJTMainView, MJTRTSView 에 사용할 <선택> Callback 함수 작성
  const handleStickerChoice = (mjtStickerInfo) => {
    setStickerPreviewUrl(mjtStickerInfo.url);
  };
  return (
    <div className="wrapper">
      <div className="chat-list">
        <img src={require('./img/chat_list.svg')} alt="background-chatlist" />
      </div>
      <div className="chat-data-wrapper">
        <div className="chat">
          <img src={require('./img/chat.svg')} alt="background-chat" />
          <div className='sticker-preview-wrapper'>
            {stickerPreviewUrl ? (
              <img
                src={stickerPreviewUrl}
                alt='sticker-preview'
                className='sticker-preview'
              />
            ) : null}
          </div>
        </div>
        <div className="input-view-wrapper">
          <button onClick={() => setIsShow(!isShow)} className={isShow ? 'main-view-btn main-view-btn-selected' : 'main-view-btn'}>
            😀
          </button>
          <div className="rts-view-wrapper">
            <div className="mjt-rts-view">
              {
                // 6. Add MJTRTSView Component
                // 6. MJTRTSView Component 추가
              }
              <MJTRTSView
                text={text}
                width="inherit"
                height="64px"
                onStickerChoice={(mjtStickerInfo) => handleStickerChoice(mjtStickerInfo)}
              />
            </div>
            <div className="input-wrapper">
              <textarea
                id="name"
                name="name"
                placeholder="Enter Text"
                colum={2}
                value={text}
                className={isShow ? 'input main-view-show-input' : 'input'}
                onChange={(e) => setText(e.target.value)}
              />
            </div>
          </div>
        </div>
      </div>
      {isShow ? (
        <div className="mjt-main-view-wrapper">
          {
            // 5. Add MJTMainView Component
            // 5. MJTMainView Component 추가
          }
          <MJTMainView width={350} height={300} onStickerChoice={(mjtStickerInfo) => handleStickerChoice(mjtStickerInfo)} />
        </div>
      ) : null}
    </div>

    /* 7. Register <chosen> sticker as a SentEvent when <send> event is made
      ..(example omitted).. // When the sticker is chosen, the callback function is
      called, and Mojitok.addSendEvent(..) must be called on the received meta information. */
    /* 7. <선택>된 스티커의 meta정보를 <전송>시에 SentEvent로 등록
      ..(예제 생략).. // SDK에서 <선택>된 스티커의 meta정보를 App에서 넘겨받아,
      // 유저가 실제로 <전송>했을 때, 해당정보에 대해 Mojitok.addSendEvent(..) 호출 */
  );
};

export default App;
